import React from "react";
import { Cursor, useTypewriter } from "react-simple-typewriter";
import BackgroundCircles from "./BackgroundCircles";
import { Link, Router } from "react-router-dom";

type Props = {};

export default function Hero({}: Props) {
  const [text, count] = useTypewriter({
    words: ["Hi, I am Aaryan Mittal", "A Passionate Developer"],
    loop: true,
    delaySpeed: 2000,
  });
  return (
    <div className="h-screen flex flex-col space-y-8 items-center justify-center text-center overflow-hidden">
      <BackgroundCircles />
      <img
        className="relative rounded-full h-32 w-32 mx-auto object-cover"
        src="https://i.ibb.co/02m4cg3/aaryan.jpg"
        alt="aaryan"
      ></img>
      <div>
        <h2 className="text-sm uppercase text-black-500 pb-2 tracking-[15px] font-semibold">
          Full Stack Software Engineer
        </h2>
        <h1 className="text-5xl lg:text-67xl font-semibold scroll-px-10">
          <span className="mr-3">{text}</span>
          <Cursor cursorColor="#F7AB0A" />
        </h1>
        {/* <div className="pt-5">
          <button className="heroButton">About</button>
          <button className="heroButton">Experience</button>
          <button className="heroButton">Skills</button>
          <button className="heroButton">Projects</button>
        </div> */}
      </div>
    </div>
  );
}
