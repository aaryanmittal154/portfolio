import React from "react";
import { motion } from "framer-motion";

type Props = {};

export default function About({}: Props) {
  return (
    <motion.div className="flex flex-col relative h-screen text-center max-w-7xl px-10 justify-evenly mx-auto items-center">
      <motion.img
        className="-mb-20 flex-shrink-0 w-56 h-56 rounded-full object-cover xl:w-500px xl:h-600px"
        initial={{
          x: -200,
          opacity: 0,
        }}
        transition={{
          duration: 1.2,
        }}
        whileInView={{ opacity: 1, x: 0 }}
        viewport={{ once: true }}
        src="https://i.ibb.co/02m4cg3/aaryan.jpg"
      />
      <div className="space-y-2 px-0">
        <h4 className="text-4xl font-semibold">AARYAN MITTAL</h4>
        <h6 className="text-2xl text-gray-500">FULL STACK SOFTWARE ENGINEER</h6>
        <p className="justify-content uppercase justify-text text-align-justify">
          As a software engineer with a lifelong passion for technology and an
          unrelenting drive to excel, I recently graduated from Hack Reactor
          with a thirst for new challenges and opportunities to test my skills
          and knowledge. My eagerness to learn and collaborate with others has
          made me proficient in various technologies and frameworks. I'm quick
          on my feet, always up for a challenge, and dedicated to producing work
          of the highest quality.
        </p>
        <p className="justify-content uppercase justify-text text-align-justify">
          My education and experience have equipped me with the tools necessary
          to thrive in any software engineering role. I'm eager to continue my
          growth and development in this exciting and constantly evolving field.
          I can bring a unique and valuable perspective to any team.
        </p>
        <p className="justify-content uppercase justify-text text-align-justify">
          I look forward to connecting with potential employers and colleagues
          who share my passion for technology. If you're interested in learning
          more about my qualifications, please don't hesitate to reach out via
          email or LinkedIn. I'm ready and eager to explore new opportunities.
        </p>
      </div>
    </motion.div>
  );
}
