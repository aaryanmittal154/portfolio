import React from "react";
import { motion } from "framer-motion";
type Props = {};

export default function Projects({}: Props) {
  const projects = [1, 2, 3];
  return (
    <motion.div
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      transition={{ duration: 1.5 }}
      className="h-screen relative flex overflow-hidden flex-col text-left md:flex-row max-w-full justify-evenly mx-auto items-center z-0"
    >
      <h3 className="absolute top-24 uppercase tracking-[20px] text-gray-500 text-2xl">
        Main project
      </h3>
      <div>
        <div className="relative w-full flex overflow-x-scroll overflow-y-hidden snap-x snap-mandatory z-20 scrollbar scrollbar-track-gray-400/20 scrollbar-thumb-[#F7AB0A]/80">
          <div
            key="project"
            className="hover:opacity-100 w-screen flex-shrink-0 snap-center flex flex-col space-y-5 items-center justify-center p-20 md:p-44 h-screen"
          >
            <a href="https://gitlab.com/aaryanmittal154/direct-rep">
              <motion.img
                initial={{
                  y: -300,
                  opacity: 0,
                }}
                transition={{ duration: 1.2 }}
                whileInView={{ opacity: 1, y: 0 }}
                viewport={{ once: true }}
                src="https://i.ibb.co/WKRSZJm/Screenshot-2023-01-12-at-5-33-20-PM.png"
                alt=""
              />
            </a>
            <div className="">
              <h4 className="text-4xl font-semibold text-center ">
                <span className="underline decoration-[#F7AB0A]/50">
                  {" "}
                  {/* Case Study 1 of {projects.length}:{" "} */}
                  DirectREP
                </span>{" "}
              </h4>
              <p className="text-lg text-center md:text-left text-gray-500">
                DirectREP utilizes AI technology to assist citizens in
                expressing their concerns to their elected representatives
                through the medium of a letter. The application uses AI
                generated letters to make it easier for individuals to clearly
                and effectively communicate their views to their locally elected
                officials.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="w-full absolute top-[30%] bg-[#f7AB0A]/10 left-0 h-[500px] -skew-y-12"></div>
    </motion.div>
  );
}
