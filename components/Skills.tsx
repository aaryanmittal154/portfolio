import React from "react";
import { motion } from "framer-motion";
import Skill from "./Skill";

type Props = {};

export default function Skills({}: Props) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      transition={{ duration: 1 }}
      className="h-screen flex relative flex-col text-center md:text-left xl:flex-row max-w-[2000px] xl:px-10 min-h-screen justify-center xl:space-y-0 mx-auto items-center"
    >
      <h3 className="absolute top-24 uppercase tracking-[20px] text-gray-500 text-2xl">
        Skills
      </h3>
      <h3 className="absolute top-36 uppercase tracking-[3px] text-gray-500 text-sm">
        Hover over a skill for currency proficiency
      </h3>

      <div className="grid grid-cols-4 gap-5">
        <div className="group relative flex cursor-pointer">
          <motion.img
            initial={{
              x: -200,
              opacity: 0,
            }}
            transition={{ duration: 1 }}
            whileInView={{ opacity: 1, x: 0 }}
            src="https://thumbs.dreamstime.com/b/python-logo-icon-vector-logos-logo-icons-set-social-media-flat-banner-vectors-svg-eps-jpg-jpeg-emblem-wallpaper-background-python-208329675.jpg"
            className="rounded-full border border-gray-500 object-cover w-24 h-24 filter group-hover:grayscale transition duration-300 ease-in-out"
          />
          <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 rounded-full z-0">
            <div className="flex  items-center justify-center h-full">
              <p className="text 3-xl font-bold text-black opacity-100">
                PYTHON
              </p>
            </div>
          </div>
        </div>
        <div className="group relative flex cursor-pointer">
          <motion.img
            initial={{
              x: -200,
              opacity: 0,
            }}
            transition={{ duration: 1 }}
            whileInView={{ opacity: 1, x: 0 }}
            src="https://thumbs.dreamstime.com/b/javascript-logo-editorial-illustrative-white-background-javascript-logo-editorial-illustrative-white-background-eps-download-208329455.jpg"
            className="rounded-full border border-gray-500 object-cover w-24 h-24 filter group-hover:grayscale transition duration-300 ease-in-out"
          />
          <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 rounded-full z-0">
            <div className="flex  items-center justify-center h-full">
              <p className="text 3-xl font-bold text-black opacity-100">
                JAVASCRIPT
              </p>
            </div>
          </div>
        </div>
        <div className="group relative flex cursor-pointer">
          <motion.img
            initial={{
              x: -200,
              opacity: 0,
            }}
            transition={{ duration: 1 }}
            whileInView={{ opacity: 1, x: 0 }}
            src="https://dc722jrlp2zu8.cloudfront.net/media/fbads-fastapi.jpg"
            className="rounded-full border border-gray-500 object-cover w-24 h-24 filter group-hover:grayscale transition duration-300 ease-in-out"
          />
          <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 rounded-full z-0">
            <div className="flex  items-center justify-center h-full">
              <p className="text 3-xl font-bold text-black opacity-100">
                FASTAPI
              </p>
            </div>
          </div>
        </div>
        <div className="group relative flex cursor-pointer">
          <motion.img
            initial={{
              x: -200,
              opacity: 0,
            }}
            transition={{ duration: 1 }}
            whileInView={{ opacity: 1, x: 0 }}
            src="https://mpng.subpng.com/20180711/rtc/kisspng-django-web-development-web-framework-python-softwa-django-5b45d913f29027.4888902515313042119936.jpg"
            className="rounded-full border border-gray-500 object-cover w-24 h-24 filter group-hover:grayscale transition duration-300 ease-in-out"
          />
          <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 rounded-full z-0">
            <div className="flex  items-center justify-center h-full">
              <p className="text 3-xl font-bold text-black opacity-100">
                DJANGO
              </p>
            </div>
          </div>
        </div>
        <div className="group relative flex cursor-pointer">
          <motion.img
            initial={{
              x: -200,
              opacity: 0,
            }}
            transition={{ duration: 1 }}
            whileInView={{ opacity: 1, x: 0 }}
            src="https://download.logo.wine/logo/React_(web_framework)/React_(web_framework)-Logo.wine.png"
            className="rounded-full border border-gray-500 object-cover w-24 h-24 filter group-hover:grayscale transition duration-300 ease-in-out"
          />
          <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 rounded-full z-0">
            <div className="flex  items-center justify-center h-full">
              <p className="text 3-xl font-bold text-black opacity-100">
                REACT
              </p>
            </div>
          </div>
        </div>
        <div className="group relative flex cursor-pointer">
          <motion.img
            initial={{
              x: -200,
              opacity: 0,
            }}
            transition={{ duration: 1 }}
            whileInView={{ opacity: 1, x: 0 }}
            src="https://www.maxpixel.net/static/photo/1x/Icon-Html5-Logo-Html-2582748.png"
            className="rounded-full border border-gray-500 object-cover w-24 h-24 filter group-hover:grayscale transition duration-300 ease-in-out"
          />
          <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 rounded-full z-0">
            <div className="flex  items-center justify-center h-full">
              <p className="text 3-xl font-bold text-black opacity-100">HTML</p>
            </div>
          </div>
        </div>
        <div className="group relative flex cursor-pointer">
          <motion.img
            initial={{
              x: -200,
              opacity: 0,
            }}
            transition={{ duration: 1 }}
            whileInView={{ opacity: 1, x: 0 }}
            src="https://colorlib.com/wp/wp-content/uploads/sites/2/creative-css3-tutorials.jpg"
            className="rounded-full border border-gray-500 object-cover w-24 h-24 filter group-hover:grayscale transition duration-300 ease-in-out"
          />
          <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 rounded-full z-0">
            <div className="flex  items-center justify-center h-full">
              <p className="text 3-xl font-bold text-black opacity-100">CSS</p>
            </div>
          </div>
        </div>
        <div className="group relative flex cursor-pointer">
          <motion.img
            initial={{
              x: -200,
              opacity: 0,
            }}
            transition={{ duration: 1 }}
            whileInView={{ opacity: 1, x: 0 }}
            src="https://w7.pngwing.com/pngs/627/244/png-transparent-docker-logo-logos-logos-and-brands-icon-thumbnail.png"
            className="rounded-full border border-gray-500 object-cover w-24 h-24 filter group-hover:grayscale transition duration-300 ease-in-out"
          />
          <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 rounded-full z-0">
            <div className="flex  items-center justify-center h-full">
              <p className="text 3-xl font-bold text-black opacity-100">
                DOCKER
              </p>
            </div>
          </div>
        </div>
        <div className="group relative flex cursor-pointer">
          <motion.img
            initial={{
              x: -200,
              opacity: 0,
            }}
            transition={{ duration: 1 }}
            whileInView={{ opacity: 1, x: 0 }}
            src="https://w7.pngwing.com/pngs/396/90/png-transparent-postgresql-database-logo-computer-icons-replication-software-developer-miscellaneous-blue-mammal.png"
            className="rounded-full border border-gray-500 object-cover w-24 h-24 filter group-hover:grayscale transition duration-300 ease-in-out"
          />
          <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 rounded-full z-0">
            <div className="flex  items-center justify-center h-full">
              <p className="text 3-xl font-bold text-black opacity-100">
                POSTGRESQL
              </p>
            </div>
          </div>
        </div>
        <div className="group relative flex cursor-pointer">
          <motion.img
            initial={{
              x: -200,
              opacity: 0,
            }}
            transition={{ duration: 1 }}
            whileInView={{ opacity: 1, x: 0 }}
            src="https://www.seekpng.com/png/detail/117-1177155_mongodb-logo-mongodb-logo-transparent.png"
            className="rounded-full border border-gray-500 object-cover w-24 h-24 filter group-hover:grayscale transition duration-300 ease-in-out"
          />
          <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 rounded-full z-0">
            <div className="flex  items-center justify-center h-full">
              <p className="text 3-xl font-bold text-black opacity-100">
                MONGODB
              </p>
            </div>
          </div>
        </div>
        <div className="group relative flex cursor-pointer">
          <motion.img
            initial={{
              x: -200,
              opacity: 0,
            }}
            transition={{ duration: 1 }}
            whileInView={{ opacity: 1, x: 0 }}
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA3lBMVEX////iQyn8bSb8oybhMw32zcjgQCn8pSb8bCT8ah/8YADiPiL8aSbiQSbrUij8aybgLwD8nADhOhz0YSf8ZhL8oRjhNxb98vDgMQn0wbv93NLqUCj7lW/+7un7ekL7ooL7nib7cyb65OH31NDkVT/ytq/qhXjnalnpe23sj4T1yMPvo5ryXif8xrX6kyb8zb75fyb96NnlXUnyuLHnbVztmY/odWbwq6P53tvjSjLlW0f8sJb8h1j8v6v8t6D7fkv7dTf5dRD7v4H82b37pDj7tW380a38y6D7qEX838sijp3sAAAGD0lEQVR4nO2aiXrTOBRGKyd1m9VZnKRb2tIVutB2ykBgWAoUCu//QqMsSrxItiwplpPvPw9wc3/pWL62s7EBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgDKjEyNlTkZGypjnqtZqkU3tMqcfW63aroF+jHPaahDSaJ2aKaO/UuY5qxFK7R/NMpeTMo2PRnoyyqhFJrSGWmUGrIyuC+Z505m21v+gVea8Py3TuTDUlzlma6+r6VTS8SYa6ssYu2XCWhtolBnssDL9R2O9mWG+9qR/rlHmsU/MuGCcEyYpbe1So867+UKRlpnxwRTX87XX0nSwWCjS+ddgf/rUGovWNK6gx/KiTKNjsD9tNncWnZHaO+U672uBOuUizTWvg52pazoILhSpvTfaoxbhzkhZVdPdMjGyUub50A91pqxpSFLt8cgkZ41QZ6SsuPjhhSKNM7NtqjNqkUhCtce7q3KkTmHGbzZ0654R4fOK0nljuFNVInIR1bE5uoVUBsOdKrLL6UxF05ikyrqb5jIqF9X0tUKdC04dnRnXGCfRc2ZMX6FQpxGvU4jx+zp+GVK9rjLX2dzh1OlfL6HjrDQ4S6+i6UX0RB7TqC2h44xwl56QncyFeCrQOvbH79hNTFFTwUopnVlGGcSP+AmZ35XFxga2VLbH73O+XNkvIO7lTDRf+5ggOnTPyXgBnfIltT9+x4buhabZZkqRpPSWaPdLlLixjKdpX+SC7fG7JmyM7GR59BFKavuWyBm61dY+wQW74zdn6F6sPclQ6KPYBavj91B4zozpn2/Kcp7ggvYHOx24Q3cgYlmWlDr2xu8ktwxi74PwaaKkBrH2RkowdJvH1vgtGrqXgKXxWzR0LwFL4/envCSlmn6yEXDUct2cArqulTdSB7530yRLT0l/oNn1/CMLCSvbDqVLQy4zH403/pnt7fwD7lWdGd39ZWV097vsR6p7uSf8z3fm1JeR0XWb3uIn/K+5J/xccQLUm6YzhvI5TuVz7gmrThjP6D66+/VIfT/vgINoQno9GjtzXNKNVa/mPdbsxRM6jiFV3Sandu5HDTch3UYTxDfQRsI7n9eG42hfje4+v3D1riAJdU3lGjrGzzsh31J9U7mGWrFUnFDjvuEST1g194QjcUJlU4WGThLm/nI/KaGiqUJDJwnzDjh7tBDhNbJuo0uiU0wICw8XB5WkhjKbmmgopXKQe8IvotsFo5sloptoKMX/knvCk8QLcYy8qW5DfIbOqFp4jfGUoqkjbWqaoZTKYf4BN76maUq5kYno3qQXsvAAzH1+ilNPfaJKOUNn5P7sNOFAYhNTTRUN2mH8/E/SMUOZTUwxVcZQStXSF8QjqU10PKGpcobSLbTxtnTMwE+caxYITJUzlM4zvrX/Rd3KeSowVdJQ6uitrYCyh43DO1NlDbV2zMw4TL/tz4g8NMoaaulmv2DgS0cMmSptqFOxdxFOGcpHDJqaNmgHAtr7qwmLWJGOyEyVN7QIAamo32SPm5mp8oY6/jfb/5+dclCVvC9OTZU2dLtq9RQNcluVNtVryt4knIrN+2CU4ZOkqfX7rXvJiP5TAS7BAEcypnr1t1ulrbde6hP92FBbs6iQu/TbhvfqeKtUKm0dv0rdxkIZyhgepphKDS1NSTW1aIYyEk2dGMpIMbV4hjLuxGfqzNB5xARTC2koQ2jqwtBSmqn+YTENZXBNDRmabGoBz9AoHFMjhiaZWsn9U68CMVPr95x4U6KmFt1QRshUz+MYyjd1BQxlBEwVGMozdSUMZcxNjZ+hsYzM1FUxlDExlXuGxk2teytlKIOa6jmJhgZM9VbKUMbg8LtEvGnG74fFeJbPyo+2ZML2D9utqvKz3ZMK+NN2o+oMn9Mj9p5X6wyNkmpq+5ftFnX5nRyx/dt2g/okmdp7frDdnhF+ibZx9Q1lCExdB0MZD8dxU3vH62EoI2bq+hjKeAlHbL/Ybsg8QVN7pfUylDE3df0MZbxM5tTeOhrKePjTbrf/rKehjOHf1Z6zAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQPH5H/mFe5RAq03ZAAAAAElFTkSuQmCC"
            className="rounded-full border border-gray-500 object-cover w-24 h-24 filter group-hover:grayscale transition duration-300 ease-in-out"
          />
          <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 rounded-full z-0">
            <div className="flex  items-center justify-center h-full">
              <p className="text 3-xl font-bold text-black opacity-100">
                GITLAB
              </p>
            </div>
          </div>
        </div>
        <div className="group relative flex cursor-pointer">
          <motion.img
            initial={{
              x: -200,
              opacity: 0,
            }}
            transition={{ duration: 1 }}
            whileInView={{ opacity: 1, x: 0 }}
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQHbwipsu1SNYLVKMUzjsOdwYFBRPyPP1--O0G_rzi_7wAY4eGP6rGwOXQGIBxlD0UmPzg&usqp=CAU"
            className="rounded-full border border-gray-500 object-cover w-24 h-24 filter group-hover:grayscale transition duration-300 ease-in-out"
          />
          <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 rounded-full z-0">
            <div className="flex  items-center justify-center h-full">
              <p className="text 3-xl font-bold text-black opacity-100">
                VSCODE
              </p>
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
}
