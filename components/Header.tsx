import React from "react";
import { SocialIcon } from "react-social-icons";
import { motion } from "framer-motion";

type Props = {};

export default function Header({}: Props) {
  return (
    <header className="sticky top-0 p-5 flex items-start justify-between max-w-7xl mx-auto z-20 xl:items-center">
      <motion.div
        initial={{
          x: -500,
          opacity: 0,
          scale: 0.5,
        }}
        animate={{
          x: 0,
          opacity: 1,
          scale: 1,
        }}
        transition={{
          duration: 1,
        }}
        className="flex flex-row items-center"
      >
        {/* Social Icons */}
        <SocialIcon
          url="https://gitlab.com/aaryanmittal154"
          fgColor="black"
          bgColor="transparent"
        />
        <SocialIcon
          url="https://www.linkedin.com/in/aaryanmittal/"
          fgColor="black"
          bgColor="transparent"
        />
        <SocialIcon
          url="https://www.instagram.com/aaryanmittal_/"
          fgColor="black"
          bgColor="transparent"
        />
      </motion.div>
      <motion.div
        initial={{
          x: 500,
          opacity: 0,
          scale: 0.5,
        }}
        animate={{
          x: 0,
          opacity: 1,
          scale: 1,
        }}
        transition={{
          duration: 1,
        }}
        className="flex flex-row items-center text-gray-300 cursor-pointer"
      >
        <div className="hover-container">
          <SocialIcon
            className="cursor-pointer"
            url="https://pdfhost.io/v/NCjQb0e9Z_v"
            network="email"
            fgColor="black"
            bgColor="transparent"
          />
          <p className="hover-text ">RESUME</p>
        </div>
      </motion.div>
    </header>
  );
}
